package com.uniontech.litepaltest;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.uniontech.litepaltest.model.Book;

import org.litepal.LitePal;
import org.litepal.crud.DataSupport;
import org.litepal.crud.LitePalSupport;
import org.litepal.tablemanager.Connector;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button createDatabase = findViewById(R.id.btn_create_database);
        Button addData = findViewById(R.id.btn_add_data);
        Button updateData = findViewById(R.id.btn_update_data);
        Button deleteDate =findViewById(R.id.btn_delete_data);
        Button queryData = findViewById(R.id.btn_query_data);

        // 创建数据库
        createDatabase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Connector.getDatabase();
                Toast.makeText(MainActivity.this,"创建成功",Toast.LENGTH_SHORT).show();
            }
        });

        // 添加数据
        addData.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Book book = new Book();
                book.setName("第一行代码");
                book.setAuthor("郭霖");
                book.setPages(570);
                book.setPrice(49.99);
                book.setPress("人民邮电出版社");

                boolean save = book.save();
                if (save) {
                    Toast.makeText(MainActivity.this,"添加成功！",Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity.this,"添加失败！",Toast.LENGTH_SHORT).show();
                }
            }
        });

        // 更新数据
        updateData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Book book = new Book();
//                book.setName("深入理解Java虚拟机");
//                book.setAuthor("周志明");
//                book.setPages(387);
//                book.setPrice(69.99);
//                book.setPress("机械工业出版社");
//                book.save();
//                book.setPrice(59.99);
//                book.save();
                book.setPrice(79.99);
                book.updateAll("name = ?","深入理解Java虚拟机");
            }
        });

        // 删除数据
        deleteDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LitePal.deleteAll(Book.class,"price > ?", "50");
            }
        });

        // 查询数据
        queryData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 查询Book表中所有数据
                List<Book> books = LitePal.findAll(Book.class);
                for (Book book : books) {
                    Log.d(TAG, "Book name is " + book.getName());
                    Log.d(TAG, "Book author is " + book.getAuthor());
                    Log.d(TAG, "Book pages is " + book.getPages());
                    Log.d(TAG, "Book price is " + book.getPrice());
                    Log.d(TAG, "Book press is " + book.getPress());
                }
            }
        });
    }
}
